﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace GimpiesWinForms
{
    public partial class ManagerForm : Form
    {
        private readonly Database _database;
        private List<int> _deleteList;
        private string _currentMenu;
        private readonly int _currentId;

        public ManagerForm(string username)
        {
            _database = new Database();
            _deleteList = new List<int>();
            _currentMenu = "shoes";

            InitializeComponent();
            signedInAs.Text = $"Signed in as: {username}";
            _currentId = _database.GetId(username);
        }

        private void logoutButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dataGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            switch (_currentMenu)
            {
                case "sales":
                    e.Cancel = true;

                    break;
                case "users":
                {
                    if (e.ColumnIndex == 0) e.Cancel = true;
                    if (Convert.ToInt32(dataGridView.Rows[e.RowIndex].Cells[0].Value) == _currentId) e.Cancel = true;

                    break;
                }
                default:
                    switch (e.ColumnIndex)
                    {
                        case 0:
                        case 4:
                            e.Cancel = true;
                            break;
                    }

                    break;
            }
        }

        private void dataGridView_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (e.RowIndex <= 1) return;
            foreach (DataGridViewCell cell in dataGridView.Rows[e.RowIndex - 1].Cells)
            {
                if (cell.ColumnIndex == 0 || cell.ValueType == typeof(string)) continue;
                cell.Value = 0;
            }

            AddStyle();
        }

        private void dataGridView_Sorted(object sender, EventArgs e)
        {
            _deleteList = new List<int>();
            AddStyle();
        }

        private void dataGridView_UserAddedRow(object sender, EventArgs e)
        {
            AddStyle();
        }

        private void dataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show(e.Exception.Message);
        }

        private void managerForm_Load(object sender, EventArgs e)
        {
            ShowMenu();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            switch (_currentMenu)
            {
                case "shoes":
                {
                    foreach (DataGridViewRow row in dataGridView.Rows) _database.UpdateShoe(row);
                    foreach (var delete in _deleteList) _database.RemoveShoe(delete);
                    break;
                }
                case "users":
                {
                    foreach (DataGridViewRow row in dataGridView.Rows) _database.UpdateUser(row);
                    foreach (var delete in _deleteList) _database.RemoveUser(delete);
                    break;
                }
            }

            ShowMenu();
        }

        private void refreshButton_Click(object sender, EventArgs e)
        {
            ShowMenu();
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewCell selectedCell in dataGridView.SelectedCells)
            {
                if (!selectedCell.Selected) continue;
                if (Convert.ToInt32(selectedCell.OwningRow.Cells[0].Value) == _currentId &&
                    _currentMenu == "users") continue;

                _deleteList.Add(Convert.ToInt32(selectedCell.OwningRow.Cells[0].Value));

                foreach (DataGridViewCell cell in dataGridView.Rows[selectedCell.RowIndex].Cells)
                {
                    cell.Style.BackColor = Color.LightGray;
                    cell.Style.ForeColor = SystemColors.GrayText;
                }
            }
        }

        private void shoesButton_Click(object sender, EventArgs e)
        {
            _currentMenu = "shoes";
            ShowMenu();
        }

        private void usersButton_Click(object sender, EventArgs e)
        {
            _currentMenu = "users";
            ShowMenu();
        }

        private void salesButton_Click(object sender, EventArgs e)
        {
            _currentMenu = "sales";
            ShowMenu();
            MessageBox.Show($"Total profit: {_database.GetTotalSales():0.00}");
        }

        private void ShowMenu()
        {
            _deleteList = new List<int>();
            switch (_currentMenu)
            {
                case "shoes":
                    ManageControl(true);
                    dataGridView.DataSource = _database.GetShoes(0);

                    break;
                case "users":
                    ManageControl(true);
                    dataGridView.DataSource = _database.GetUsers(0);

                    break;
                case "sales":
                    ManageControl(false);
                    dataGridView.DataSource = _database.GetSales(0);

                    break;
            }

            dataGridView.Columns[0].Visible = false;
            AddStyle();
        }

        private void AddStyle()
        {
            if (dataGridView.Rows.Count == 0) return;
            var drop = false;

            if (_currentMenu != "sales")
            {
                foreach (DataGridViewCell cell in dataGridView.Rows[^1].Cells)
                {
                    cell.Style.BackColor = Color.SeaGreen;
                    cell.Style.ForeColor = Color.White;
                }
            }

            foreach (DataGridViewRow row in dataGridView.Rows)
            {
                foreach (DataGridViewCell cell in row.Cells)
                {
                    if (drop) continue;
                    cell.Style.BackColor = Color.MistyRose;
                    cell.Style.ForeColor = Color.Black;
                }

                drop = !drop;
                if (Convert.ToInt32(row.Cells[0].Value) == _currentId && _currentMenu == "users")
                {
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        cell.Style.BackColor = Color.LightGray;
                        cell.Style.ForeColor = SystemColors.GrayText;
                    }
                }

                if (_currentMenu != "shoes") continue;

                row.Cells[4].Style.BackColor = Color.LightGray;
                row.Cells[4].Style.ForeColor = SystemColors.GrayText;

                if (Convert.ToInt32(row.Cells[4].Value) <= 10)
                {
                    row.Cells[4].Style.BackColor = Color.Orange;
                    row.Cells[4].Style.ForeColor = Color.White;
                }

                if (Convert.ToInt32(row.Cells[4].Value) > 5) continue;
                
                row.Cells[4].Style.BackColor = Color.Red;
                row.Cells[4].Style.ForeColor = Color.White;
            }

            if (_currentMenu == "sales") return;
            foreach (DataGridViewCell cell in dataGridView.Rows[^1].Cells) cell.Style.BackColor = Color.SeaGreen;
        }

        private void ManageControl(bool active)
        {
            saveButton.Enabled = active;
            deleteButton.Enabled = active;
            dataGridView.AllowUserToAddRows = active;
            dataGridView.AllowUserToDeleteRows = active;
        }
    }
}