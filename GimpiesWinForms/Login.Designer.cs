﻿using System.Windows.Forms.VisualStyles;

namespace GimpiesWinForms
{
    partial class LoginForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginForm));
            this.loginButton = new System.Windows.Forms.Button();
            this.panel = new System.Windows.Forms.Panel();
            this.passwordBox = new System.Windows.Forms.TextBox();
            this.usernameBox = new System.Windows.Forms.TextBox();
            this.headerLabel = new System.Windows.Forms.Label();
            this.footerLabel = new System.Windows.Forms.Label();
            this.alertLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // loginButton
            // 
            this.loginButton.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.loginButton.FlatAppearance.BorderSize = 3;
            this.loginButton.Location = new System.Drawing.Point(82, 163);
            this.loginButton.Name = "loginButton";
            this.loginButton.Size = new System.Drawing.Size(130, 36);
            this.loginButton.TabIndex = 1;
            this.loginButton.Text = "Login";
            this.loginButton.UseVisualStyleBackColor = true;
            this.loginButton.Click += new System.EventHandler(this.loginButton_Click);
            // 
            // panel
            // 
            this.panel.BackgroundImage = ((System.Drawing.Image) (resources.GetObject("panel.BackgroundImage")));
            this.panel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel.Location = new System.Drawing.Point(12, -4);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(106, 84);
            this.panel.TabIndex = 0;
            this.panel.TabStop = true;
            // 
            // passwordBox
            // 
            this.passwordBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.passwordBox.Location = new System.Drawing.Point(46, 134);
            this.passwordBox.Name = "passwordBox";
            this.passwordBox.Size = new System.Drawing.Size(202, 20);
            this.passwordBox.TabIndex = 3;
            this.passwordBox.Text = "password..";
            this.passwordBox.UseSystemPasswordChar = true;
            this.passwordBox.GotFocus += new System.EventHandler(this.passwordBox_GotFocus);
            this.passwordBox.LostFocus += new System.EventHandler(this.passwordBox_LostFocus);
            // 
            // usernameBox
            // 
            this.usernameBox.Location = new System.Drawing.Point(46, 108);
            this.usernameBox.MaxLength = 20;
            this.usernameBox.Name = "usernameBox";
            this.usernameBox.Size = new System.Drawing.Size(202, 20);
            this.usernameBox.TabIndex = 2;
            this.usernameBox.Text = "enter username here..";
            this.usernameBox.GotFocus += new System.EventHandler(this.usernameBox_GotFocus);
            this.usernameBox.LostFocus += new System.EventHandler(this.usernameBox_LostFocus);
            // 
            // headerLabel
            // 
            this.headerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.headerLabel.Location = new System.Drawing.Point(124, 13);
            this.headerLabel.Name = "headerLabel";
            this.headerLabel.Size = new System.Drawing.Size(98, 26);
            this.headerLabel.TabIndex = 7;
            this.headerLabel.Text = "gimpies.nl";
            // 
            // footerLabel
            // 
            this.footerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.footerLabel.Location = new System.Drawing.Point(124, 39);
            this.footerLabel.Name = "footerLabel";
            this.footerLabel.Size = new System.Drawing.Size(170, 21);
            this.footerLabel.TabIndex = 8;
            this.footerLabel.Text = "goedkoop loopt lekker licht";
            // 
            // alertLabel
            // 
            this.alertLabel.ForeColor = System.Drawing.Color.DarkRed;
            this.alertLabel.Location = new System.Drawing.Point(12, 83);
            this.alertLabel.Name = "alertLabel";
            this.alertLabel.Size = new System.Drawing.Size(270, 22);
            this.alertLabel.TabIndex = 9;
            this.alertLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LoginForm
            // 
            this.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(304, 211);
            this.Controls.Add(this.alertLabel);
            this.Controls.Add(this.footerLabel);
            this.Controls.Add(this.headerLabel);
            this.Controls.Add(this.usernameBox);
            this.Controls.Add(this.passwordBox);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.loginButton);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon) (resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(310, 240);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(310, 240);
            this.Name = "LoginForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gimpies - Login";
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private System.Windows.Forms.Label alertLabel;

        private System.Windows.Forms.Label footerLabel;

        private System.Windows.Forms.Label headerLabel;

        private System.Windows.Forms.TextBox passwordBox;
        private System.Windows.Forms.TextBox usernameBox;

        private System.Windows.Forms.Panel panel;

        private System.Windows.Forms.Button loginButton;

        #endregion
    }
}