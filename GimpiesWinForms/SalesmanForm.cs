﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace GimpiesWinForms
{
    public partial class SalesmanForm : Form
    {
        private readonly Database _database;

        public SalesmanForm(string username)
        {
            _database = new Database();
            InitializeComponent();
            signedInAs.Text = $"Signed in as: {username}";
        }

        private void logoutButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dataGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            switch (e.ColumnIndex)
            {
                case 0:
                    if (dataGridView.RowCount - 1 == e.RowIndex) e.Cancel = true;

                    break;
                default:
                    e.Cancel = true;

                    break;
            }
        }

        private void dataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (Convert.ToInt32(dataGridView.Rows[e.RowIndex].Cells[0].Value) >
                    Convert.ToInt32(dataGridView.Rows[e.RowIndex].Cells[5].Value))
                {
                    dataGridView.Rows[e.RowIndex].Cells[0].Value = dataGridView.Rows[e.RowIndex].Cells[5].Value;
                }
                
                if (!ValidInteger(Convert.ToInt32(dataGridView.Rows[e.RowIndex].Cells[0].Value)))
                {
                    dataGridView.Rows[e.RowIndex].Cells[0].Value = 0;
                }
            }
            catch (Exception exception)
            {
                dataGridView.Rows[e.RowIndex].Cells[0].Value = 0;
                MessageBox.Show(exception.Message);
            }
        }

        private void dataGridView_Sorted(object sender, EventArgs e)
        {
            AddStyle();
        }

        private void dataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show(e.Exception.Message);
        }

        private void salesmanForm_Load(object sender, EventArgs e)
        {
            dataGridView.DataSource = _database.GetShoes(0);
            dataGridView.Columns.Add("sellAmount", "sell_amount");
            dataGridView.Columns[5].Visible = false;

            ShowMenu();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dataGridView.Rows)
            {
                if (Convert.ToInt32(row.Cells[0].Value) >= 1)
                {
                    _database.UpdateSale(row);
                }
            }
            
            ShowMenu();
        }

        private void refreshButton_Click(object sender, EventArgs e)
        {
            ShowMenu();
        }

        private static bool ValidInteger(int checkInteger)
        {
            if (checkInteger == 0) return false;
            return checkInteger == Math.Abs(checkInteger);
        }

        private void ShowMenu()
        {
            dataGridView.DataSource = _database.GetShoes(0);

            AddStyle();
        }

        private void AddStyle()
        {
            if (dataGridView.Rows.Count == 0) return;
            var drop = false;

            foreach (DataGridViewRow row in dataGridView.Rows)
            {
                row.Cells[0].Value = 0;
                foreach (DataGridViewCell cell in row.Cells)
                    cell.Style.BackColor = !drop ? Color.MistyRose : Color.White;

                drop = !drop;
                if (Convert.ToInt32(row.Cells[5].Value) <= 10)
                {
                    row.Cells[5].Style.BackColor = Color.Orange;
                    row.Cells[5].Style.ForeColor = Color.White;
                }

                if (Convert.ToInt32(row.Cells[5].Value) > 5) continue;
                row.Cells[5].Style.BackColor = Color.Red;
                row.Cells[5].Style.ForeColor = Color.White;
            }

            foreach (DataGridViewRow row in dataGridView.Rows)
            {
                row.Cells[0].Style.BackColor = Color.SeaGreen;
                row.Cells[0].Style.ForeColor = Color.White;
                row.Cells[1].Style.BackColor = Color.LightGray;
                row.Cells[1].Style.ForeColor = SystemColors.GrayText;
            }

            foreach (DataGridViewCell cell in dataGridView.Rows[^1].Cells)
            {
                cell.Value = null;
                cell.Style.BackColor = drop ? Color.MistyRose : Color.White;
            }
        }
    }
}