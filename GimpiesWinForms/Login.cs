﻿using System;
using System.Data;
using System.Diagnostics;
using System.Windows.Forms;

namespace GimpiesWinForms
{
    public partial class LoginForm : Form
    {
        private readonly Database _database;

        public LoginForm()
        {
            _database = new Database();
            InitializeComponent();
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            var validUsername = _database.CheckUsername(0, usernameBox.Text);
            var correctPassword = _database.CheckPassword(usernameBox.Text, passwordBox.Text);

            alertLabel.Text = "";

            if (!validUsername)
            {
                alertLabel.Text = "Invalid username!";
            }
            else if (!correctPassword)
            {
                alertLabel.Text = "Invalid password!";
            }

            if (!validUsername || !correctPassword) return;

            switch (_database.GetRole(usernameBox.Text))
            {
                case 4:
                    Form managerForm = new ManagerForm(_database.GetUsername(usernameBox.Text));
                    managerForm.FormClosing += delegate { Show(); };
                    managerForm.Show();
                    Hide();
                    
                    break;
                case 3:
                    Form salesmanForm = new SalesmanForm(_database.GetUsername(usernameBox.Text));
                    salesmanForm.FormClosing += delegate { Show(); };
                    salesmanForm.Show();
                    Hide();
                    
                    break;
                case 2:
                    var gimpiesConsole = new GimpiesConsole.Program();
                    Process.Start($"{gimpiesConsole.ReturnPath()}\\GimpiesConsole.exe");
                    Close();
                    
                    break;
                default:
                    var result = MessageBox.Show("Not permitted to enter. \n\nClick OK to exit.", "Gimpies", MessageBoxButtons.OK);
                    
                    if (result == DialogResult.OK)
                    {
                        Close();
                    }
                    
                    break;
            }
        }

        private void usernameBox_GotFocus(object sender, EventArgs e)
        {
            if (usernameBox.Text != "enter username here..") return;
            usernameBox.Text = "";
        }

        private void usernameBox_LostFocus(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(usernameBox.Text)) return;
            usernameBox.Text = "enter username here..";
        }

        private void passwordBox_GotFocus(object sender, EventArgs e)
        {
            if (passwordBox.Text != "password..") return;
            passwordBox.Text = "";
        }

        private void passwordBox_LostFocus(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(passwordBox.Text)) return;
            passwordBox.Text = "password..";
        }
    }
}