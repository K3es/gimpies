﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace GimpiesWinForms
{
    internal class Database
    {
        private MySqlConnection _connection;

        public Database()
        {
            Initialize();
        }

        private void Initialize()
        {
            const string connectionString = "SERVER=localhost; DATABASE=gimpies; UID=root; PASSWORD=;";
            _connection = new MySqlConnection(connectionString);
        }

        private MySqlConnection Connection()
        {
            switch (_connection.State)
            {
                case ConnectionState.Open:
                    return _connection;
                case ConnectionState.Closed:
                    try
                    {
                        _connection.Open();
                        return _connection;
                    }
                    catch (MySqlException exception)
                    {
                        Console.WriteLine(exception);
                        return null;
                    }
            }

            return _connection;
        }

        private MySqlCommand CreateCommand(string query)
        {
            return new MySqlCommand(query, Connection());
        }

        public void UpdateShoe(DataGridViewRow row)
        {
            var command =
                CreateCommand(
                    "UPDATE shoes SET brand=@brand, model=@model, size=@size, stock=@stock, buy_price=@buy_price, sell_price=@sell_price WHERE id=@id");

            using (command)
            {
                if (!CheckStock(Convert.ToInt32(row.Cells[0].Value)))
                {
                    AddShoe(row);
                    return;
                }

                command.Parameters.AddWithValue("id", row.Cells[0].Value);
                command.Parameters.AddWithValue("brand", row.Cells[1].Value);
                command.Parameters.AddWithValue("model", row.Cells[2].Value);
                command.Parameters.AddWithValue("size", row.Cells[3].Value);
                command.Parameters.AddWithValue("stock", row.Cells[4].Value);
                command.Parameters.AddWithValue("buy_price", row.Cells[5].Value);
                command.Parameters.AddWithValue("sell_price", row.Cells[6].Value);

                command.ExecuteNonQuery();
            }
        }

        public void UpdateUser(DataGridViewRow row)
        {
            var command = CreateCommand(
                GetPassword(Convert.ToInt32(row.Cells[0].Value)) == Convert.ToString(row.Cells[2].Value)
                    ? "UPDATE users SET username=@username, role_id=@role_id, street_name=@street_name, home_address=@home_address, zip_code=@zip_code, country=@country WHERE id=@id"
                    : "UPDATE users SET username=@username, password=@password, role_id=@role_id, street_name=@street_name, home_address=@home_address, zip_code=@zip_code, country=@country WHERE id=@id");

            using (command)
            {
                if (row.Cells[1].Value == null) return;
                if (!CheckUsername(Convert.ToInt32(row.Cells[0].Value), ""))
                {
                    AddUser(row);
                    return;
                }

                if (GetPassword(Convert.ToInt32(row.Cells[0].Value)) != Convert.ToString(row.Cells[2].Value))
                {
                    command.Parameters.AddWithValue("password", HashPassword(Convert.ToString(row.Cells[2].Value)));
                }

                command.Parameters.AddWithValue("id", row.Cells[0].Value);
                command.Parameters.AddWithValue("username", row.Cells[1].Value);
                command.Parameters.AddWithValue("role_id", row.Cells[3].Value);
                command.Parameters.AddWithValue("street_name", row.Cells[4].Value);
                command.Parameters.AddWithValue("home_address", row.Cells[5].Value);
                command.Parameters.AddWithValue("zip_code", row.Cells[6].Value);
                command.Parameters.AddWithValue("country", row.Cells[7].Value);

                command.ExecuteNonQuery();
            }
        }

        public void UpdateSale(DataGridViewRow row)
        {
            var command =
                CreateCommand(
                    "UPDATE sales SET brand=@brand, model=@model, size=@size, sold=@sold, profit=@profit WHERE id=@id");

            using (command)
            {
                if (!CheckSale(Convert.ToInt32(row.Cells[1].Value)))
                {
                    AddSale(row);
                    return;
                }
                
                var id = row.Cells[1].Value;
                RemoveStock(Convert.ToInt32(id), Convert.ToInt32(row.Cells[0].Value));
                
                command.Parameters.AddWithValue("id", id);
                command.Parameters.AddWithValue("brand", row.Cells[2].Value);
                command.Parameters.AddWithValue("model", row.Cells[3].Value);
                command.Parameters.AddWithValue("size", row.Cells[4].Value);
                command.Parameters.AddWithValue("sold",
                    GetSold(Convert.ToInt32(id)) + Convert.ToInt32(row.Cells[0].Value));
                command.Parameters.AddWithValue("profit",
                    GetProfit(Convert.ToInt32(id), GetSold(Convert.ToInt32(id)) + Convert.ToInt32(row.Cells[0].Value)));

                command.ExecuteNonQuery();
            }
        }

        public void RemoveShoe(int id)
        {
            var command =
                CreateCommand("DELETE FROM shoes WHERE id=@id");

            using (command)
            {
                command.Parameters.AddWithValue("id", id);

                command.ExecuteNonQuery();
            }
        }

        public void RemoveUser(int id)
        {
            var command =
                CreateCommand("DELETE FROM users WHERE id=@id");

            using (command)
            {
                command.Parameters.AddWithValue("id", id);

                command.ExecuteNonQuery();
            }
        }

        private void RemoveStock(int id, int stock)
        {
            var command = CreateCommand("UPDATE shoes SET stock=@stock WHERE id=@id");

            using (command)
            {
                command.Parameters.AddWithValue("stock", GetStock(id) - stock);
                command.Parameters.AddWithValue("id", id);

                command.ExecuteNonQuery();
            }
        }

        private void AddShoe(DataGridViewRow row)
        {
            if (row.Cells[1].Value == null)
            {
                return;
            }

            var command =
                CreateCommand(
                    "INSERT INTO shoes(brand, model, size, stock, buy_price, sell_price) VALUES(@brand, @model, @size, @stock, @buy_price, @sell_price)");

            using (command)
            {
                command.Parameters.AddWithValue("brand", row.Cells[1].Value);
                command.Parameters.AddWithValue("model", row.Cells[2].Value);
                command.Parameters.AddWithValue("size", row.Cells[3].Value);
                command.Parameters.AddWithValue("stock", row.Cells[4].Value);
                command.Parameters.AddWithValue("buy_price", row.Cells[5].Value);
                command.Parameters.AddWithValue("sell_price", row.Cells[6].Value);

                command.ExecuteNonQuery();
            }
        }

        private void AddUser(DataGridViewRow row)
        {
            var command = CreateCommand(
                GetPassword(Convert.ToInt32(row.Cells[0].Value)) == Convert.ToString(row.Cells[2].Value)
                    ? "INSERT INTO users(username, role_id, street_name, home_address, zip_code, country) VALUES(@username, @role_id, @street_name, @home_address, @zip_code, @country)"
                    : "INSERT INTO users(username, password, role_id, street_name, home_address, zip_code, country) VALUES(@username, @password, @role_id, @street_name, @home_address, @zip_code, @country)");

            using (command)
            {
                if (GetPassword(Convert.ToInt32(row.Cells[0].Value)) != Convert.ToString(row.Cells[2].Value))
                {
                    command.Parameters.AddWithValue("password", HashPassword(Convert.ToString(row.Cells[2].Value)));
                }

                command.Parameters.AddWithValue("username", row.Cells[1].Value);
                command.Parameters.AddWithValue("role_id", row.Cells[3].Value);
                command.Parameters.AddWithValue("street_name", row.Cells[4].Value);
                command.Parameters.AddWithValue("home_address", row.Cells[5].Value);
                command.Parameters.AddWithValue("zip_code", row.Cells[6].Value);
                command.Parameters.AddWithValue("country", row.Cells[7].Value);

                command.ExecuteNonQuery();
            }
        }

        private void AddSale(DataGridViewRow row)
        {
            var command =
                CreateCommand(
                    "INSERT INTO sales(id, brand, model, size, sold, profit) VALUES(@id, @brand, @model, @size, @sold, @profit)");

            using (command)
            {
                var id = row.Cells[1].Value;
                RemoveStock(Convert.ToInt32(id), Convert.ToInt32(row.Cells[0].Value));
                
                command.Parameters.AddWithValue("id", id);
                command.Parameters.AddWithValue("brand", row.Cells[2].Value);
                command.Parameters.AddWithValue("model", row.Cells[3].Value);
                command.Parameters.AddWithValue("size", row.Cells[4].Value);
                command.Parameters.AddWithValue("sold",
                    GetSold(Convert.ToInt32(id)) + Convert.ToInt32(row.Cells[0].Value));
                command.Parameters.AddWithValue("profit",
                    GetProfit(Convert.ToInt32(id), GetSold(Convert.ToInt32(id)) + Convert.ToInt32(row.Cells[0].Value)));

                command.ExecuteNonQuery();
            }
        }

        private int GetStock(int id)
        {
            var command = CreateCommand("SELECT * FROM shoes WHERE id=@id");

            using (command)
            {
                command.Parameters.AddWithValue("id", id);

                var dataReader = command.ExecuteReader();

                using (dataReader)
                {
                    while (dataReader.Read())
                    {
                        return (dataReader.GetInt32(4));
                    }
                }
            }

            return 0;
        }

        public DataTable GetShoes(int id)
        {
            var command = CreateCommand(id >= 1 ? "SELECT * FROM shoes WHERE id=@id" : "SELECT * FROM shoes");

            using (command)
            {
                command.Parameters.AddWithValue("id", id);

                var dataReader = command.ExecuteReader();

                using (dataReader)
                {
                    var dataTable = new DataTable();
                    dataTable.Load(dataReader);

                    return dataTable;
                }
            }
        }

        public DataTable GetUsers(int id)
        {
            var command = CreateCommand(id >= 1 ? "SELECT * FROM users WHERE id=@id" : "SELECT * FROM users");

            using (command)
            {
                command.Parameters.AddWithValue("id", id);

                var dataReader = command.ExecuteReader();

                using (dataReader)
                {
                    var dataTable = new DataTable();
                    dataTable.Load(dataReader);

                    return dataTable;
                }
            }
        }

        public DataTable GetSales(int id)
        {
            var command = CreateCommand(id >= 1 ? "SELECT * FROM sales WHERE id=@id" : "SELECT * FROM sales");

            using (command)
            {
                command.Parameters.AddWithValue("id", id);

                var dataReader = command.ExecuteReader();

                using (dataReader)
                {
                    var dataTable = new DataTable();
                    dataTable.Load(dataReader);

                    return dataTable;
                }
            }
        }
        
        public double GetTotalSales()
        {
            var command = CreateCommand("SELECT profit FROM sales");
            double totalSales = 0;

            using (command)
            {
                var dataReader = command.ExecuteReader();

                using (dataReader)
                {
                    while (dataReader.Read())
                    {
                        totalSales += dataReader.GetDouble(0);
                    }
                }
            }

            return totalSales;
        }

        public int GetId(string username)
        {
            var command = CreateCommand("SELECT id FROM users WHERE username=@username");

            using (command)
            {
                command.Parameters.AddWithValue("username", username);

                var dataReader = command.ExecuteReader();

                using (dataReader)
                {
                    while (dataReader.Read())
                    {
                        return dataReader.GetInt32(0);
                    }
                }
            }

            return 0;
        }

        private double GetProfit(int id, int sold)
        {
            if (sold == 0) sold = 1;
            var command = CreateCommand("SELECT buy_price, sell_price FROM shoes WHERE id=@id");

            using (command)
            {
                command.Parameters.AddWithValue("id", id);

                var dataReader = command.ExecuteReader();

                using (dataReader)
                {
                    while (dataReader.Read())
                    {
                        var marginDouble = dataReader.GetDouble(1) - dataReader.GetDouble(0);
                        marginDouble = Convert.ToDouble($"{marginDouble:0.00}") - 0.01;

                        return marginDouble * sold;
                    }
                }
            }

            return 0;
        }

        private int GetSold(int id)
        {
            var command = CreateCommand("SELECT sold FROM sales WHERE id=@id");

            using (command)
            {
                command.Parameters.AddWithValue("id", id);

                var dataReader = command.ExecuteReader();

                using (dataReader)
                {
                    while (dataReader.Read())
                    {
                        return dataReader.GetInt32(0);
                    }
                }
            }

            return 0;
        }

        public string GetUsername(string username)
        {
            var command = CreateCommand("SELECT username FROM users WHERE username=@username");

            using (command)
            {
                command.Parameters.AddWithValue("username", username);

                var dataReader = command.ExecuteReader();

                using (dataReader)
                {
                    while (dataReader.Read())
                    {
                        return dataReader.GetString(0);
                    }
                }
            }

            return "";
        }

        private string GetPassword(int id)
        {
            var command = CreateCommand("SELECT password FROM users WHERE id=@id");

            using (command)
            {
                command.Parameters.AddWithValue("id", id);

                var dataReader = command.ExecuteReader();

                using (dataReader)
                {
                    while (dataReader.Read())
                    {
                        return dataReader.GetString(0);
                    }
                }
            }

            return "";
        }

        public int GetRole(string username)
        {
            var command = CreateCommand("SELECT role_id FROM users WHERE username=@username");
            command.Parameters.AddWithValue("username", username);

            var dataReader = command.ExecuteReader();

            using (dataReader)
            {
                while (dataReader.Read())
                {
                    return dataReader.GetInt32(0);
                }
            }

            return 0;
        }

        private bool CheckStock(int id)
        {
            var command = CreateCommand("SELECT * FROM shoes WHERE id=@id");

            using (command)
            {
                command.Parameters.AddWithValue("id", id);

                var dataReader = command.ExecuteReader();

                using (dataReader)
                {
                    while (dataReader.Read())
                    {
                        return Convert.ToBoolean(dataReader.GetInt32(0));
                    }
                }
            }

            return false;
        }

        public bool CheckUsername(int id, string username)
        {
            var command = CreateCommand(id == 0
                ? "SELECT EXISTS(SELECT username FROM users WHERE username=@username)"
                : "SELECT EXISTS(SELECT username FROM users WHERE id=@id)");

            using (command)
            {
                if (id == 0)
                {
                    command.Parameters.AddWithValue("username", username);
                }
                else
                {
                    command.Parameters.AddWithValue("id", id);
                }


                var dataReader = command.ExecuteReader();

                using (dataReader)
                {
                    while (dataReader.Read())
                    {
                        return Convert.ToBoolean(dataReader.GetInt32(0));
                    }

                    return false;
                }
            }
        }

        private bool CheckSale(int id)
        {
            var command = CreateCommand("SELECT * FROM sales WHERE id=@id");

            using (command)
            {
                command.Parameters.AddWithValue("id", id);

                var dataReader = command.ExecuteReader();

                using (dataReader)
                {
                    while (dataReader.Read())
                    {
                        return Convert.ToBoolean(dataReader.GetInt32(0));
                    }
                }
            }

            return false;
        }

        public bool CheckPassword(string username, string password)
        {
            var command = CreateCommand("SELECT password FROM users WHERE username=@username");

            using (command)
            {
                command.Parameters.AddWithValue("username", username);

                var dataReader = command.ExecuteReader();

                using (dataReader)
                {
                    while (dataReader.Read())
                    {
                        return VerifyPassword(password, dataReader.GetString(0));
                    }

                    return false;
                }
            }
        }

        private static string HashPassword(string password)
        {
            return BCrypt.Net.BCrypt.HashPassword(password);
        }

        private static bool VerifyPassword(string password, string hash)
        {
            return BCrypt.Net.BCrypt.Verify(password, hash);
        }
    }
}