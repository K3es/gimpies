-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 06, 2020 at 03:32 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gimpies`
--

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` int(4) NOT NULL,
  `brand` varchar(20) NOT NULL,
  `model` varchar(20) NOT NULL,
  `size` int(4) NOT NULL,
  `sold` int(4) NOT NULL,
  `profit` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id`, `brand`, `model`, `size`, `sold`, `profit`) VALUES
(1, 'Nike', 'Air-Max', 33, 46, 459.54),
(2, 'Puma', 'Axelion', 26, 8, 79.92),
(3, 'Puma', 'Mercedes', 45, 62, 619.38),
(4, 'Adidas', 'Ultraboost', 37, 1, 9.99);

-- --------------------------------------------------------

--
-- Table structure for table `shoes`
--

CREATE TABLE `shoes` (
  `id` int(11) NOT NULL,
  `brand` varchar(20) DEFAULT NULL,
  `model` varchar(20) DEFAULT NULL,
  `size` int(4) DEFAULT 20,
  `stock` int(4) DEFAULT 0,
  `buy_price` double DEFAULT 19.99,
  `sell_price` double DEFAULT 29.99
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `shoes`
--

INSERT INTO `shoes` (`id`, `brand`, `model`, `size`, `stock`, `buy_price`, `sell_price`) VALUES
(1, 'Nike', 'Air-Max', 33, 4, 59.99, 69.99),
(2, 'Puma', 'Axelion', 26, 86, 29.99, 39.99),
(3, 'Puma', 'Mercedes', 45, 1056, 49.99, 59.99),
(4, 'Adidas', 'Ultraboost', 37, 2416, 69.99, 79.99),
(5, 'Nike', 'Air-Plus', 20, 100, 59.99, 69.99),
(6, 'Nike', 'Air', 35, 7, 39.99, 49.99);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(4) NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role_id` int(4) DEFAULT 1,
  `street_name` varchar(40) DEFAULT NULL,
  `home_address` int(4) DEFAULT NULL,
  `zip_code` varchar(7) DEFAULT NULL,
  `country` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role_id`, `street_name`, `home_address`, `zip_code`, `country`) VALUES
(1, 'manager', '$2a$11$vD7yzjA5mRzTa6RGu6ESJ.NV6ku1O7Q4y400y549Mi5oF4q0uvfPy', 4, 'Bastion', 124, '6901NZ', 'NL'),
(2, 'purchaser', '$2a$11$1lmgkc6rb1Yg8XfIeyYHbeoRaVprb4PrzF4SmEXO5klAFXJhUci.y', 2, 'Holder', 56, '6904EK', 'NL'),
(3, 'salesman', '$2a$11$LLrQGob3PuX4Y1JrEAxlQOTJ.MlmwNyzaqxaP4Vo0kVDXpdcJWYT.', 3, 'Holder', 7, '6902JA', 'NL'),
(4, 'customer', '$2a$11$rbOvWBWgM..Btn.AHIHcMeKb3A2G8HhBFuB14f92r57X.bdluDsXa', 1, 'Schaapsdrift', 69, '6902AB', 'NL'),
(5, 'm', '$2a$11$OE9VRQjPEx.1/3heXAOp9eY8QwA7U83Or3f6lUK7rzJWxdDZBDZgS', 4, 'Holder', 1, '0000ZZ', 'DE');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `shoes`
--
ALTER TABLE `shoes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `shoes`
--
ALTER TABLE `shoes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
