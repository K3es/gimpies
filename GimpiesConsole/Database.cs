﻿using System;
using MySql.Data.MySqlClient;
using System.Data;

namespace GimpiesConsole
{
    internal class Database
    {
        private MySqlConnection _connection;

        public Database()
        {
            Initialize();
        }

        private void Initialize()
        {
            const string connectionString = "SERVER=localhost; DATABASE=gimpies; UID=root; PASSWORD=;";
            _connection = new MySqlConnection(connectionString);
        }

        private MySqlConnection Connection()
        {
            switch (_connection.State)
            {
                case ConnectionState.Open:
                    return _connection;
                case ConnectionState.Closed:
                    try
                    {
                        _connection.Open();
                        return _connection;
                    }
                    catch (MySqlException exception)
                    {
                        Console.WriteLine(exception);
                        return null;
                    }
            }

            return _connection;
        }

        private MySqlCommand CreateCommand(string query)
        {
            return new MySqlCommand(query, Connection());
        }

        public void AddStock(int id, int stock)
        {
            var command = CreateCommand("UPDATE shoes SET stock=@stock WHERE id=@id");

            using (command)
            {
                command.Parameters.AddWithValue("stock", stock + GetStock(id));
                command.Parameters.AddWithValue("id", id);

                command.ExecuteNonQuery();
            }
        }

        public DataTable GetStocks(int id)
        {
            var command = CreateCommand(id >= 1 ? "SELECT * FROM shoes WHERE id=@id" : "SELECT * FROM shoes");

            using (command)
            {
                command.Parameters.AddWithValue("id", id);
                
                var dataReader = command.ExecuteReader();

                using (dataReader)
                {
                    var dataTable = new DataTable();
                    dataTable.Load(dataReader);
                    
                    return dataTable;
                }
            }
        }
        
        private int GetStock(int id)
        {
            var command = CreateCommand("SELECT * FROM shoes WHERE id=@id");

            using (command)
            {
                command.Parameters.AddWithValue("id", id);
                
                var dataReader = command.ExecuteReader();

                using (dataReader)
                {
                    while (dataReader.Read())
                    {
                        return (dataReader.GetInt32(4));
                    }
                }
            }

            return 0;
        }

        public int GetId(string username)
        {
            var command = CreateCommand("SELECT id FROM users WHERE username=@username");

            using (command)
            {
                command.Parameters.AddWithValue("username", username);

                var dataReader = command.ExecuteReader();

                using (dataReader)
                {
                    while (dataReader.Read())
                    {
                        return (dataReader.GetInt32(0));
                    }
                }
            }

            return 0;
        }

        public bool CheckUser(string username)
        {
            var command = CreateCommand("SELECT EXISTS(SELECT username FROM users WHERE username=@username)");

            using (command)
            {
                command.Parameters.AddWithValue("username", username);

                var dataReader = command.ExecuteReader();

                using (dataReader)
                {
                    while (dataReader.Read())
                    {
                        return (dataReader.GetInt32(0) == 1);
                    }

                    return false;
                }
            }
        }

        public bool CheckPassword(string username, string password)
        {
            var command = CreateCommand("SELECT username, password FROM users WHERE username=@username");

            using (command)
            {
                command.Parameters.AddWithValue("username", username);

                var dataReader = command.ExecuteReader();

                using (dataReader)
                {
                    while (dataReader.Read())
                    {
                        return VerifyPassword(password, dataReader.GetString(1));
                    }

                    return false;
                }
            }
        }

        public bool CheckRole(string username, int roleId)
        {
            var command = CreateCommand("SELECT role_id FROM users WHERE username=@username AND role_id>=@role_id");
            command.Parameters.AddWithValue("username", username);
            command.Parameters.AddWithValue("role_id", roleId);

            var dataReader = command.ExecuteReader();

            using (dataReader)
            {
                while (dataReader.Read())
                {
                    return (dataReader.GetInt32(0) >= roleId);
                }
            }

            return false;
        }
        
        private static bool VerifyPassword(string password, string hash)
        {
            return BCrypt.Net.BCrypt.Verify(password, hash);
        }
    }
}