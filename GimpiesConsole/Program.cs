﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace GimpiesConsole
{
    public class Program
    {
        private Database _database;
        private static List<MenuItem> _menuItems;

        private static void Main(string[] args)
        {
            new Program().Run();
        }

        private void Run()
        {
            _database = new Database();

            /*
            _database.AddUser()
            _database.AddShoe()
            _database.AddStock()
            
            _database.CheckUser()
            _database.CheckPassword()
            _database.CheckRole()
            
            _database.GetStocks()
            _database.GetId()
            */

            var userId = LoginMenu();
            
            if (userId < 1) return;
            StockMenu();
        }

        // Initiates the login menu.
        private int LoginMenu()
        {
            int menuChoice;

            _menuItems = new List<MenuItem>
            {
                new MenuItem {Description = "Login", Execute = RequestCredentials},
                new MenuItem {Description = "Exit", Execute = Exit}
            };

            do
            {
                Console.Clear();
                Console.WriteLine("Please select a menu:");

                for (var i = 0; i < _menuItems.Count; i++)
                {
                    Console.WriteLine($"[{i + 1}] {_menuItems[i].Description}");
                }
            } while (!int.TryParse(Console.ReadLine(), out menuChoice) ||
                     menuChoice < 1 || menuChoice > _menuItems.Count);

            Console.Clear();
            var result = _menuItems[menuChoice - 1].Execute();

            var loggedIn = Convert.ToBoolean(result[0]);
            var username = result[1];

            if (!loggedIn)
            {
                Console.WriteLine($"{username ?? "Failed to login."}");
                Exit();
            }

            Console.WriteLine($"Logged into {username}.");

            if (_database.CheckRole(username, 3)) return _database.GetId(username);
            Console.WriteLine("However, you do not have permission to supply stocks.");
            Exit();

            return _database.GetId(username);
        }

        // Handles requesting of the username and password for a possible login attempt.
        private List<string> RequestCredentials()
        {
            var username = "";
            var correctPassword = "";

            var loginAttempts = 4;

            while (!_database.CheckUser(username) && loginAttempts > 1)
            {
                loginAttempts -= 1;
                Console.Clear();

                Console.WriteLine($"Please enter a valid username:\n\n\n{loginAttempts} attempts left");
                Console.SetCursorPosition(0, Console.CursorTop - 3);

                username = Console.ReadLine();
            }

            if (loginAttempts == 1)
            {
                Console.Clear();
                return new List<string> {Convert.ToString("false"), "Invalid username. Exceeded allowed attempts."};
            }

            loginAttempts = 4;

            while (!_database.CheckPassword(username, correctPassword) && loginAttempts > 1)
            {
                loginAttempts -= 1;
                Console.Clear();

                Console.WriteLine($"Username: {username}");
                Console.WriteLine($"Please enter a password:\n\n\n{loginAttempts} attempts left");
                Console.SetCursorPosition(0, Console.CursorTop - 3);

                while (true)
                {
                    var key = Console.ReadKey(true);

                    if (key.Key == ConsoleKey.Enter)
                    {
                        break;
                    }

                    switch (key.Key)
                    {
                        case ConsoleKey.Backspace:
                        case ConsoleKey.Delete:
                        {
                            if (Console.CursorLeft < 1) continue;
                            Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);
                            Console.Write(' ');
                            Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);

                            correctPassword = correctPassword?.Remove(correctPassword.Length - 1);
                            break;
                        }
                        case ConsoleKey.UpArrow:
                        case ConsoleKey.DownArrow:
                        case ConsoleKey.LeftArrow:
                        case ConsoleKey.RightArrow:
                            break;
                        default:
                            Console.Write("*");
                            correctPassword += key.KeyChar;
                            break;
                    }
                }
            }

            var isLoggedIn = _database.CheckPassword(username, correctPassword);

            if (!isLoggedIn)
            {
                Console.Clear();
                return new List<string> {Convert.ToString("false"), "Invalid password. Exceeded allowed attempts."};
            }

            Console.Clear();
            return new List<string> {"true", username};
        }

        // Initiates the stock menu.
        private void StockMenu()
        {
            int menuChoice;
            _menuItems = new List<MenuItem>
            {
                new MenuItem {Description = "Get Stock", Execute = GetStock},
                new MenuItem {Description = "Add Stock", Execute = AddStock},
                new MenuItem {Description = "Exit", Execute = Exit}
            };

            do
            {
                Console.Clear();
                Console.WriteLine("Please select a menu:");

                for (var i = 0; i < _menuItems.Count; i++)
                {
                    Console.WriteLine($"[{i + 1}] {_menuItems[i].Description}");
                }
            } while (!int.TryParse(Console.ReadLine(), out menuChoice) ||
                     menuChoice < 1 || menuChoice > _menuItems.Count);

            Console.Clear();
            var result = _menuItems[menuChoice - 1].Execute();
        }

        // GetStock for use in the stock menu.
        private List<string> GetStock()
        {
            RequestStock(0);
            
            BackToStock();
            return null;
        }
        
        // AddStock for use in the stock menu.
        private List<string> AddStock()
        {
            RequestStock(0);
            Console.WriteLine("Please determine the ID of stock to change (0 to go back):");
            var id = Convert.ToInt32(Console.ReadLine());
            
            Console.Clear();
            if (id == 0) StockMenu();
            Console.WriteLine("Selected stock:");
            
            RequestStock(id);
            Console.WriteLine("Stock to add:");
            var amount = Convert.ToInt32(Console.ReadLine());
            
            _database.AddStock(id, amount);
            Console.Clear();
            Console.WriteLine("Stock added!");
            RequestStock(0);
            BackToStock();
            return null;
        }

        // Writes out the stock.
        private void RequestStock(int id)
        {
            var stockTable = _database.GetStocks(id);
            var labelText = stockTable.Columns.Cast<DataColumn>()
                .Aggregate("", (current, column) => current + $"| {column.ColumnName,-11}");
            var switchColor = true;

            Console.ForegroundColor = ConsoleColor.Black;
            Console.BackgroundColor = ConsoleColor.White;
            Console.WriteLine(labelText);
            Console.ResetColor();

            foreach (DataRow row in stockTable.Rows)
            {
                var tableEntry = row.ItemArray.Aggregate("", (current, item) => current + $"| {item,-11}");
                ConsoleColor backgroundColor;

                if (!switchColor)
                {
                    backgroundColor = ConsoleColor.White;

                    switchColor = true;
                }
                else
                {
                    backgroundColor = ConsoleColor.Gray;

                    switchColor = false;
                }

                Console.ForegroundColor = Convert.ToInt32(row["Stock"]) < 10 ? ConsoleColor.DarkRed : ConsoleColor.Black;
                Console.BackgroundColor = backgroundColor;

                Console.WriteLine(tableEntry);

                if (Convert.ToInt32(row["Stock"]) < 5)
                {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.SetCursorPosition(tableEntry.Length, Console.CursorTop - 1);
                    Console.WriteLine(" < STOCK BELOW 5. ");
                }

                Console.ResetColor();
            }
        }

        // Sends you back to the stock menu, instead of exiting the application abruptly.
        private List<string> BackToStock()
        {
            Console.WriteLine("Press enter to go back..");
            Console.ReadLine();
            
            StockMenu();
            return null;
        }

        // Exits the environment after pressing 'enter.'
        private static List<string> Exit()
        {
            Console.WriteLine("Press enter to exit..");
            Console.ReadLine();
            
            Environment.Exit(0);
            return null;
        }
        
        public string ReturnPath()
        {
            return Environment.CurrentDirectory;
        }
    }
}