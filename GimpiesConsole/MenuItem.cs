﻿using System;
using System.Collections.Generic;

namespace GimpiesConsole
{
    public class MenuItem
    {
        public string Description { get; set; }
        public Func<List<string>> Execute { get; set; }
    }
}